import { Redirect } from 'umi'
import { userLocalSotre } from "@/pages/login"
export default (props) => {
  try {
    const userInfo = userLocalSotre.get();
    const expires_in = userInfo?.expires_in;
    if(expires_in > Date.now()) { // 没有过期
      // 用户menulist
      return props.children;
    } 
    return <Redirect to="/login" />
  }catch(error){
    return <Redirect to="/login" />
  }

}