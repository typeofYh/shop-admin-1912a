import { request } from "umi";
import { PageParams } from "./pagenation"

export const getProdList = (params:PageParams) => request('/api/prod/prod/page', {
  method: 'get',
  params
})

// 获取产品分类
export const getCategoryList = () => request('/api/prod/category/listCategory')

// 获取产品分组
export const getTagList = () => request('/api/prod/prodTag/listTagList')

// 运费设置
export const getTransportList = () => request('/api/shop/transport/list')

// 根据id获取运费模板
export const getTransportId = (id) => request('/api/shop/transport/info/' + id)

// 上传文件
export const uploadProdImage = (data : FormData) => request('/api/admin/file/upload/element', {
  method: 'POST',
  data
})

// 获取产品规格
export const getSkuList = () => request('/api/prod/spec/list')

// 根据规格id获取规格值
export const getSkuData = (id:number) => request('/api/prod/spec/listSpecValue/' + id)