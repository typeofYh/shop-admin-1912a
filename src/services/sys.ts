import { request } from "umi";

export const getAreaList = () => request('/api/admin/area/list')

export const delAreaItem = (id:number) => request('/api/admin/area/' + id, {
  method: 'DELETE'
})

export interface AreaItemParams {
  areaId: number;  
  areaName: string;
  areas: null;
  level: number;
  parentId: number;
}

export const areaItem = (method: 'post' | 'put', data: AreaItemParams) => request('/api/admin/area', {
  method,
  data
})