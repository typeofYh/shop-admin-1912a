export interface PageParams {
  current: number;
  size: number;
}