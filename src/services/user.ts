import { request } from "umi";

// console.log(request);
export const login = (data) => request('/api/login', {
  method: 'post',
  data
})

export const getNav = () => request('/api/sys/menu/nav', {
  method: 'get'
})