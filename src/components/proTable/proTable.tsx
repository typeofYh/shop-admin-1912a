import { Table, Form } from "antd";
import { useState, useEffect } from "react"

const ProTable = ({actionRef, rowKey, pagination, columns, request, pagination = {}}) => {
  const [loading, setLoading] = useState(false);
  const [pageData, setPageData] = useState({
    page:1,
    pageSize: pagination.pageSize || 10
  });
  const [dataSource, setDataSource] = useState([]);
  const requestMethod = async (formData = {}) => {
    setLoading(true);
    const {data, success, total} = await request({...pageData,...formData})
    setDataSource(data);
    setLoading(false);
  }
  useEffect(() => {
    requestMethod();
  }, [pageData])
  return (
    <div>
      <Form onFinish={(values) => {requestMethod(values)}}>
        
      </Form>
      <Table 
        loading={loading}
        rowKey={rowKey}
        columns={columns}
        dataSource={dataSource}
        pagination={{
          ...pagination,
          onChange(page, pageSize){
            setPageData({
              page,
              pageSize
            })
          }
        }}
      />
    </div>
  )
}


const MyComponent = () => {
  return (
    <ProTable 
        actionRef={ref} 
        rowKey="prodId"
        pagination={{
          pageSize: 5
        }}
        columns={columnsConfig}
        request={request} // 表格初始 点击查询 点击分页
      />
  )
}