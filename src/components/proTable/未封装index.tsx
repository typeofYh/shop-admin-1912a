import { Table, Form } from "antd";
import { useState, useEffect } from "react"
const ProTable = () => {
  const [dataSource, setDataSource] = useState([]);
  const [total, setTatal] = useState(0);

  const requestTable = (page) => {
    const data = {
      list:[1,2,3,4],
      total: 4
    };
    setDataSource(data.list)
    setTatal(data.total)
  }
  useEffect(() => {
    requestTable(1);
  },[])

  return (
    <div>
      <Form onFinish={(values) => {
        requestTable(values)
      }}>
      </Form>
      <Table 
        dataSource={dataSource}
        pagination={{
          total,
          pageSize: 5,
          onChange: (page, limit) => {
            requestTable(page)
          }
        }}
      />
    </div>
  )
}

export default ProTable;