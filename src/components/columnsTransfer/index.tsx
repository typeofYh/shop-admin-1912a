import { Modal, Transfer } from "antd"
import { FC, useMemo, useEffect, useState } from "react"
import type { ProColumns } from '@ant-design/pro-components';

interface Iprops {
  show: boolean;
  columns: ProColumns<{notColumnShow:boolean}>[];
  modalConfig?:any;
  transferConfig?:any;
  onShowChange?:Function;
}
const Com:FC<Iprops> = (props) => {
  const [dataSource, setDataSource] = useState<any[]>([]);
  useEffect(() => {
    setDataSource(props.columns.filter(val => !val.notColumnShow).map(item => ({
      title: item.title,
      key: item.dataIndex,
      isShow: true
    })))
  }, [props.columns])
  const targetKeys = useMemo(() => {
    return dataSource.filter(val => val.isShow).map(item => item.key);
  },[dataSource]);
  const handleChange = (targetKeys, direction:string, moveKeys) => {
    dataSource.map(item => {
      if(moveKeys.includes(item.key)){
        item.isShow = (direction === 'right')
      }
    })
    setDataSource([...dataSource]);
    props.onShowChange && props.onShowChange(dataSource);
  }
  return (
    <Modal
      open={props.show}
      {...props.modalConfig}
      footer={null}
    >
      <Transfer 
        dataSource={dataSource}
        titles={['隐藏', '显示']}
        targetKeys={targetKeys}
        render={item => item.title}
        onChange={handleChange}
        {...props.transferConfig}
      />
    </Modal>
  )
}

export default Com;