// console.log('app start');
import { RequestConfig, history } from "umi"
import { message } from "antd"
import { userLocalSotre } from "@/pages/login"
import { getNav } from "@/services/user"
import LocalStore from "@/utils/storage"
const menuListStorage = new LocalStore({
  type: 'session',
  name: 'menuList'
})
const authoritiesStorage = new LocalStore({
  type: 'session',
  name: 'authorities'
})
const codeMaps = {
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

const whistList = ['/login'];
export const request: RequestConfig = { // 覆盖request的默认配置
  timeout: 10000,
  errorConfig: {
    adaptor: (resData) => { // 得到拦截器的返回值 message的提示框
      return resData;
    },
  },
  requestInterceptors: [
    async (url, options) => {
      const t = Date.now();
      options.method === 'post' ? (options.data.t = t) : (typeof options.params === 'object' && (options.params.t = t));
      const headers = {
        ...options.headers
      }
      if(whistList.findIndex(val => url.includes(val)) < 0){
        headers.Authorization = userLocalSotre.get()?.tokenType + userLocalSotre.get()?.token;
      }
      return {
        url,
        options: {
          ...options,
          headers
        }
      }
    } 
  ],
  responseInterceptors: [ // 拦截器先执行
    async (response) => {
      if(response.ok){
        return response;
      }
      if(response.status === 401) {
        message.error('您的登录状态过期请重新登录', 1, () => {
          history.replace('/login');
        });
        return {
          ...response
        }
      }
      try {
        const res = await response.text();
        return {
          ...response,
          errorMessage: res || response.statusText || codeMaps[`${response.status}`],
          showType: 1,
          success:false
        }
      }catch(error){
        return {
          ...response,
          errorMessage: '接口格式错误',
          showType: 2,
          success: false
        }
      }
    }
  ]
}


const init = async () => {
  try {
    const {menuList, authorities} = await getNav();
    menuListStorage.set(menuList)
    authoritiesStorage.set(authorities)
  }catch(error){
    console.log(error)
  }
}
const formatMenuList = (arr, parentArr, rootPath) => {
  Array.isArray(arr) && arr.forEach(item => {
      const o = {
        name: item.name,
        path: item.parentId === 0 ? '/' + item.list[0].url.split('/')[0] : ''
      }
      if(item.url) {
        o.id = item.url;
        o.path = `/${item.url}`.startsWith(rootPath) ? `/${item.url}` : `${rootPath}/${item.url}`;
        o.exact = true;
        o.component = require(`@/pages${o.path}/index.tsx`).default;
      }
      if(item.list && Array.isArray(item.list)){
        o.routes  = [];
        formatMenuList(item.list, o.routes, o.path)
      }
      parentArr.push(o);
  })
}
export function patchRoutes({ routes }) {// 合并路由
  const menulist = menuListStorage.get();
  formatMenuList(menulist, routes[0].routes, '/')
  // console.log('patchRoutes---',routes);
}
// 进入页面就会执行的
const notAddRoutePagesList = [
  '/login'
]
export async function render(oldRender) {
  // console.log('render', history);
  const { pathname } = history.location;
  if(notAddRoutePagesList.includes(pathname)){
    oldRender(); //真正的渲染视图
    return;
  }
  // 获取权限
  await init()
  oldRender(); // 渲染
}