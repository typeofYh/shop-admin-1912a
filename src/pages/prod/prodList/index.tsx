import type { ProColumns } from '@ant-design/pro-components';
import style from "./index.less"
import classNames from 'classnames';
import { ProTable } from "@ant-design/pro-components"
import { getProdList } from "@/services/prod"
import { Image, Tag, Select, Button } from "antd"
import { PlusOutlined, MenuFoldOutlined } from '@ant-design/icons';
import ColumnsTransfer from "@/components/columnsTransfer"
import { useRef, useState } from 'react';
import { history } from "umi"
interface ProdTableColumns {
  notColumnShow?: boolean
}
enum Status {
  未上架 = 0,
  上架,
}
const columns:ProColumns<ProdTableColumns>[] = [
  {
    title: '产品名称',
    dataIndex: 'prodName'
  },
  {
    title: '商品原价',
    dataIndex: 'oriPrice',
    search: false,
  },
  {
    title: '商品现价',
    dataIndex: 'price',
    search: false
  },
  {
    title: '商品库存',
    dataIndex: 'totalStocks',
    search: false
  },
  {
    title: '产品图片',
    dataIndex: 'pic',
    render(text, record, index){
      return <Image width={200} src={record.pic} />
    },
    search: false
  },
  {
    title: '状态',
    dataIndex: 'status',
    render(text, record, index){
      return <Tag>{Status[record.status]}</Tag>
    },
    renderFormItem(item,options,form){
      const handleStatusChange = (value:number) => {
        form.setFieldValue('status',value);
      }
      return <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
        <Select.Option value={Status['未上架']}>{Status[0]}</Select.Option>
        <Select.Option value={Status['上架']}>{Status[1]}</Select.Option>
      </Select>
    }
  },
  {
    title: '操作',
    search: false,
    notColumnShow: true,
    render(text, record){
      return (
        <div>
          <Button onClick={() => history.push('/prodInfo?prodid='+record?.prodId)}>修改</Button>
        </div>
      )
    }
  }
];
const Pages = () => {
  const ref = useRef();
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const request = async (arg) => {
    console.log('request',arg);
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getProdList({
      ...arg
    })
    // console.log(data);
    return {
      data:records,
      success: true,
      total
    }
  }
  const handleShowChange = (options) => {
    // console.log(options);
    const arr = columns.filter(item => options.find(val => val.key === item.dataIndex)?.isShow || item.notColumnShow)
    setColumnsConfig([...arr]);
  }
  return (
    <div>
      <ProTable 
        actionRef={ref} 
        rowKey="prodId"
        toolbar={{
          subTitle: [
            <Button
              key="add"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => {
                history.push('/prodInfo')
              }}
            >
              新增
            </Button>,
            <Button
              key="del"
              danger
              onClick={() => {
                alert('删除');
              }}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: <MenuFoldOutlined />,
              tooltip: '刷新',
              key: 'reload',
              onClick: () => {
                console.log(ref.current)
                ref.current.reload();
                // request();
              }
            },
            {
              icon: <MenuFoldOutlined />,
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              }
            }
          ]
        }}
        pagination={{
          pageSize: 5
        }}
        columns={columnsConfig}
        request={request} // 表格初始 点击查询 点击分页
      />
      <ColumnsTransfer 
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false)
          }
        }}
      />
    </div>
  )
}
export default Pages;