import { useState, useMemo } from "react";
import { v4 as uuidv4 } from "uuid"
import className from "classnames";
import {
  LoginForm,
  ProFormText
} from "@ant-design/pro-components";
import logo from "@/static/logo.jpg";
import { history } from "umi"
import style from "./style.less";
import { login } from "@/services/user"
import LocalStore from "@/utils/storage"
export const userLocalSotre = new LocalStore({
  type: 'local', // local session
  name: 'userInfo'
})
interface LoginFormData {
  principal:string;
  credentials:string;
  imageCode:string;
  sessionUUID?: string;
}
// console.log(style);
const LoginPage = () => {
  const [codeImageLoading, setCodeImageLoading] = useState(true);
  const [uuid, setUuid] = useState(() => uuidv4());
  const ImageUrl = useMemo(() => `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${uuid}`, [uuid])
  const changeCodeImage = async () => {
    setCodeImageLoading(true);
    setUuid(uuidv4());
  }
  const onFinish = async (values:LoginFormData) => {
    values = {
      ...values,
      sessionUUID: uuid
    }
    const res = await login(values);
    console.log(res);
    userLocalSotre.set({
      token: res.access_token,
      tokenType: res.token_type,
      expires_in: Date.now() + res.expires_in * 1000
    })
    // history.push('/');
    // window.location.reload();
    window.location.href = '/'; // 会重新加载页面触发 render函数
  }
  return (
    <div className={style.loginwrapper}>
      <LoginForm 
        autoComplete="off"
        logo={logo}
        title={'八维电商管理平台'}
        subTitle={'仅限1912A内部管理商品数据使用'}
        onFinish={onFinish}
      >
        <ProFormText
          name="principal"
          rules={[
            {
              required: true,
              message: '请输入用户名!',
            },
          ]}
        ></ProFormText>
        <ProFormText.Password
          name="credentials"
          rules={[
            {
              required: true,
              message: '请输入密码!',
            },
          ]}
        ></ProFormText.Password>
        <div className={style.codeWrapper}>
          <ProFormText
            className={style.codeInp}
            name="imageCode"
            placeholder={'请输入验证码'}
            rules={[
              {
                required: true,
                message: '请输入验证码!',
              },
            ]}
          ></ProFormText>
          <div 
            className={className({
              [style.loading]: codeImageLoading
            }, style.codeImage)}
          >
            <img 
              src={ImageUrl} 
              onClick={changeCodeImage}
              onError={() => {
                setCodeImageLoading(false)
              }} 
              onLoad={() => {
                setCodeImageLoading(false)
              }} 
            />
          </div>
        </div>
      </LoginForm>
    </div>
  )
}

export default LoginPage;