import { history } from "umi"
import { useEffect, useState } from "react"
import { getCategoryList, getTagList, getTransportList, getTransportId, uploadProdImage, getSkuList, getSkuData } from "@/services/prod"
import { Form, Upload, Radio, Cascader, Input, Checkbox, Button, Table, Tag } from "antd"
import deliveryTemplateColumns from "./deliveryTemplateTable"
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import {skuBaseTable, skuPicColumns} from "./skuBaseTable"
import cls from "classnames"
import style from "./index.less"
interface SKUTableValue {
  skuName?: string;
  skuImage?: string;
  price: number;
  oriPrice: number;
  totalStocks: number;
  sizeKg: number;
  sizeM: number;
}
const formatCategoryData = (data: any = [], parentId: number) => {
  return data.filter(val => Number(val.parentId) === parentId).map((item) => {
    return {
      ...item,
      children: formatCategoryData(data, item.categoryId)
    }
  })
}

const formatData = (data) => {
  const array = data.reduce((pre, item) => { // 3
    let itemArr:any[] = [];
    if(pre.length){ // 2 pre [[0],[1]]
        pre.forEach(preVal => { // preVal [{}] [{}]  item [0,1,2]
           item.skuValue.forEach(val => {
               itemArr.push({
                [item.skuName.propId]: val.propValue,
                ...preVal,
                skuName: `${preVal.skuName} ${val.propValue}`,
                skuImage: '',
                ...initSkuTable,
               }) // 6
           })
        })
    } else { // 1
       item.skuValue.forEach(val => {
          itemArr.push({
            [item.skuName.propId]: val.propValue,
            skuName: val.propValue,
            skuImage: '',
            ...initSkuTable,
          })
       })
    }
    return itemArr;
  }, [])
  console.log('array',array);
  return array;
}
const initSkuTable = {
  price: 0.01,
  oriPrice: 0.01,
  totalStocks: 0,
  sizeKg: 0,
  sizeM: 0
}
const ProdInfoPage = () => {
  const [ form ] = Form.useForm();
  const [ SkuForm ] = Form.useForm();
  const [skuTableData, setSkuTableData] = useState<any[]>([initSkuTable]);
  // 用户选择的数据
  const [ UserFormData, setUserFormData ] = useState({
    uploadImageLoading: false, // 产品图片上传loading
    deliveryMode: [], // 用户选择的配送方式
    deliveryTemplateId: null, // 配送方式下运费设置的模板id
    deliveryTemplateTable: [], // 运费设置的表格数据
    deliveryTemplateTableLoading: true, // 运费设置的表格loading
    skuFormOpen: false, // 控制规格表单显示
    skuCurData: [], //当前的商品规格值
    skuAllData: [], // 所有确定的商品规格
  });
  // 表单初始数据
  const [ formData, setFormData ] = useState({});
  const init = async (prodid:string | undefined) => {
    if(prodid){ // 修改

    }
    // 进入页面获取产品分类，产品分组，运费设置
    const [CategoryList, TagList, TransportList, SkuList] = await Promise.allSettled([
      getCategoryList(), 
      getTagList(), 
      getTransportList(),
      getSkuList()
    ]);
    setFormData({
      CategoryList: formatCategoryData(CategoryList.value, 0),
      TagList: TagList.value,
      TransportList: TransportList.value,
      SkuList: SkuList.value
    })
  }
  useEffect(() => {
    const { location } = history;
    init(location.query?.prodid)
  }, [])
  // 配送方式的选项配置
  const deliveryModeOptions = [
    { label: '商家配送', value: 'hasUserPickUp' },
    { label: '用户自提', value: 'hasShopDelivery' },
  ];
  const uploadButton = (
    <div>
      {UserFormData.uploadImageLoading ? <LoadingOutlined /> : <PlusOutlined />}
    </div>
  );
  const handleUpload = async ({file}) => {
    // 开始上传
    setUserFormData({
      ...UserFormData,
      uploadImageLoading: true
    })
    // 拿到文件对象
    const formData = new FormData();
    formData.append('file', file);
    await uploadProdImage(formData);
    console.log('上传成功');
     // 上传成功
     setUserFormData({
      ...UserFormData,
      uploadImageLoading: false
    })
  }
  // 点击添加规格按钮
  const handleClickAddSku = () => {
    setUserFormData({
      ...UserFormData,
      skuFormOpen: !UserFormData.skuFormOpen
    })
  }
  // 点击添加规格确定按钮
  const handleClickAddSkuData = async () => {
    // 校验表单
    await SkuForm.validateFields()
    // 收集表单数据
    const {skuName:[skuNameId], skuValue} = SkuForm.getFieldsValue();
    // 往skuAllData中添加规格
    const index = formData.SkuList.findIndex(val => val.propId === skuNameId);
    const skuAllData = [...UserFormData.skuAllData, {
      skuName: formData.SkuList[index],
      inputShow: false, // 控制input展示的
      skuValue: skuValue.flat(1).map((id) => UserFormData.skuCurData.find(val => val.valueId === id))
    }]
    // 添加之后删除规格
    formData.SkuList.splice(index, 1);
    // 设置组件状态
    setFormData({
      ...formData
    })
    setUserFormData({
      ...UserFormData,
      skuAllData,
      skuFormOpen: !UserFormData.skuFormOpen
    })
    // 重置表单
    SkuForm.resetFields()
  }
  // 监听用户规格数据变化
  useEffect(() => {
    console.log('UserFormData.skuAllData', UserFormData.skuAllData);
   if(UserFormData.skuAllData.length){  // 
    // UserFormData.skuAllData.skuValue
    // 添加规格
    // 表格数据变化
    setSkuTableData(formatData(UserFormData.skuAllData))
   }
    
  }, [UserFormData.skuAllData])
  return ( 
    <Form className={style.formWrapper}>
      <Form.Item 
        name='pic'
        label={'产品图片'}
      >
        <Upload
          name="prodImage"
          accept="image/png, image/jpeg, image/jpg, image/gif, image/svg"
          className={style['prodImage-uploader']}
          fileList={[]}
          customRequest={handleUpload}
        >
          { uploadButton }
        </Upload>
      </Form.Item>
      <Form.Item 
        name='status'
        label={'产品状态'}
        wrapperCol={{span: 5,}}
      >
        <Radio.Group>
          <Radio value={1}>上架</Radio>
          <Radio value={0}>下架</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item 
        name='categoryId'
        label={'产品分类'}
        wrapperCol={{span: 5,}}
      >
        <Cascader 
          options={formData?.CategoryList}
          fieldNames={{label: 'categoryName', value: 'categoryId'}}
        />
      </Form.Item>
      <Form.Item 
        name='tagList'
        label={'产品分组'}
        wrapperCol={{span: 5,}}
      >
        <Cascader 
          options={formData?.TagList}
          multiple
          fieldNames={{label: 'title', value: 'id'}}
        />
      </Form.Item>
      <Form.Item 
        name='prodName'
        rules={[{ required: true, message: '请填写产品名称' }]}
        label={'产品名称'}
        wrapperCol={{span: 5, }}
      >
        <Input />
      </Form.Item>
      <Form.Item 
        name='brief'
        label={'产品卖点'}
        wrapperCol={{span: 5, }}
      >
        <Input.TextArea />
      </Form.Item>
      <Form.Item 
        name='deliveryMode'
        label={'配送方式'}
        wrapperCol={{span: 5, }}
      >
         <Checkbox.Group options={deliveryModeOptions} onChange={(val) => {
            setUserFormData({
              ...UserFormData,
              deliveryMode: val, // [hasUserPickUp, hasShopDelivery]
              deliveryTemplateId: val.includes('hasUserPickUp') ? UserFormData.deliveryTemplateId : null,
              deliveryTemplateTable: val.includes('hasUserPickUp') ? UserFormData.deliveryTemplateTable : [],
            })
         }}/>
      </Form.Item>
      {
        /**包含商家配送，渲染运费设置 */
        UserFormData.deliveryMode.includes('hasUserPickUp') && (
        <Form.Item 
          rules={[{ required: true, message: '请选择运费设置' }]}
          label={'运费设置'}
          wrapperCol={{span: 5, }}
        >
          <Cascader 
            options={formData?.TransportList}
            placeholder="请选择运费设置模板"
            fieldNames={{label: 'transName', value: 'transportId'}}
            onChange={async (val) => {
              // 用户点击x会导致val是undefined
              if(val && Array.isArray(val)){
                // 设置用户选择值
                setUserFormData({
                  ...UserFormData,
                  deliveryTemplateTableLoading: true,
                })
                // 根据模板id请求模板数据
                const { transfees } = await getTransportId(val[0]);
                setUserFormData({
                  ...UserFormData,
                  deliveryTemplateId: val[0],
                  deliveryTemplateTable: transfees,
                  deliveryTemplateTableLoading: false,
                })
              } else { // 初始状态
                setUserFormData({
                  ...UserFormData,
                  deliveryTemplateId: null,
                  deliveryTemplateTable: [],
                  deliveryTemplateTableLoading: true,
                })
              }
            }}
          />
        </Form.Item>
        )
      }
      {
        /**运费模板的表格 */
        UserFormData.deliveryTemplateId && (
          <Table 
            dataSource={UserFormData.deliveryTemplateTable} 
            loading={UserFormData.deliveryTemplateTableLoading} 
            columns={deliveryTemplateColumns}
            pagination={false}
          />
        )
      }
      <Form.Item 
        label={'商品规格'}
        wrapperCol={{span: 5, }}
      >
        <Button onClick={handleClickAddSku} className={cls({
          [style.active]: UserFormData.skuFormOpen
        })}>添加规格</Button>
      </Form.Item>
      {
        UserFormData.skuFormOpen && <Form
          form={SkuForm}
        >
          <Form.Item 
            label="规格名" 
            wrapperCol={{span: 5,}} name="skuName"
            rules={[{ required: true, message: '请选择规格名称' }]}
          >
            <Cascader 
              options={formData?.SkuList}
              fieldNames={{label: 'propName', value:'propId'}}
              placeholder="请选择规格"
              onChange={async (val) => {
                SkuForm.setFieldValue('skuValue', []) // 切换规格把规格值设置成空
                setUserFormData({
                  ...UserFormData,
                  skuCurData: []
                })
                if(val && Array.isArray(val)){
                  // 请求成功
                  const res = await getSkuData(val[0]);
                  setUserFormData({
                    ...UserFormData,
                    skuCurData: res
                  })
                } else {
                  setUserFormData({
                    ...UserFormData,
                    skuCurData: []
                  })
                }
              }}
            />
          </Form.Item>
          <Form.Item 
            label="规格值" 
            wrapperCol={{span: 5,}} 
            name="skuValue"
            rules={[{ required: true, message: '请选择规格值' }]}
          >
            <Cascader 
              loading={UserFormData.skuCurData?.length <= 0}
              options={UserFormData.skuCurData}
              fieldNames={{label:'propValue', value:'valueId'}}
              multiple
              placeholder="请选择对应的规格值"
            />
          </Form.Item>
          <Form.Item>
            <Button onClick={handleClickAddSkuData}>确定</Button>
            <Button onClick={handleClickAddSku}>取消</Button>
          </Form.Item>
        </Form>
      }
      {
        UserFormData.skuAllData.map(({skuName, skuValue, inputShow}, index) => {
          return (
            <div key={skuName?.propId} className={style.skuList}>
              <p>
                <span>{skuName?.propName}</span>
                <Button size="small">删除</Button>
              </p>
              <div className={style.skuListForm}>
                {
                  skuValue?.map(val => (
                    <Tag key={val.valueId} closable>{val.propValue}</Tag>
                  ))
                }
                {
                  inputShow ? <Input width={60} ref={(val) => {
                    // val && val.focus() // 获取焦点
                  }} /> : (
                    <Button icon={<PlusOutlined />} onClick={() => {
                      UserFormData.skuAllData.map((item,i) => {
                        item.inputShow = (i === index);
                      })
                      setUserFormData({
                        ...UserFormData
                      })
                    }}>添加</Button>
                  )
                }
              </div>
            </div>
          )
        })
      }
      {/**规格表格 */}
      {
        <Table 
          dataSource={skuTableData}
          columns={[...UserFormData.skuAllData.map(item => ({
            title: item.skuName.propName,
            dataIndex: item.skuName.propId
          })),...(UserFormData.skuAllData.length ? skuPicColumns : []),...skuBaseTable]}
        />
      }
      <Form.Item
        label={'产品详情'}
      >
        <div></div>
      </Form.Item>
    </Form>
  )
}

export default ProdInfoPage;