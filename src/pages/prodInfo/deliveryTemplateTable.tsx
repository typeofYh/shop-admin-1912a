const columns = [
  {
    title: '配送区域',
    dataIndex: 'cityList',
    render(text, record) {
      return (
        <span>{record.cityList?.length ? record.cityList.join('/') : '所有区域'}</span>
      )
    },
  },
  {
    title: '首件(个)',
    dataIndex: 'firstFee'
  },
  {
    title: '运费（元）',
    dataIndex: 'firstPiece'
  },
  {
    title: '续件(个)',
    dataIndex: 'continuousFee'
  },
  {
    title: '续费(元)',
    dataIndex: 'continuousPiece'
  },
]

export default columns;