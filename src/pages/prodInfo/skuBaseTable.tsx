import { InputNumber, Button, Input } from "antd"
export const skuBaseTable = [
  {
    title: '销售价',
    dataIndex: 'price',
    render:(text, record) => {
      return <InputNumber value={text}/>
    }
  },
  {
    title: '市场价',
    dataIndex: 'oriPrice',
    render:(text, record) => {
      return <InputNumber value={text}/>
    }
  },
  {
    title: '库存',
    dataIndex: 'totalStocks',
    render:(text, record) => {
      return <InputNumber value={text}/>
    }
  },
  {
    title: '商品重量(kg)',
    dataIndex: 'sizeKg',
    render:(text, record) => {
      return <InputNumber value={text}/>
    }
  },
  {
    title: '商品体积(m³)',
    dataIndex: 'sizeM',
    render:(text, record) => {
      return <InputNumber value={text}/>
    }
  },
  {
    title: '操作',
    dataIndex: 'options',
    render:() => {
      return <Button>禁用</Button>
    }
  },
]

export const skuPicColumns = [
  {
    title: 'sku图片'
  },
  {
    title: '商品名称',
    render:(text, record) => {
      // console.log(record);
      return <Input.TextArea value={record.skuName}></Input.TextArea>
    }
  }
]

