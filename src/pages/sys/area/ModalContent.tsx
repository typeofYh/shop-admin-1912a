import {Form, Cascader, Input} from "antd";
import { useEffect, forwardRef, useImperativeHandle } from "react"
export interface AreaItem {
  areaId: number;
  areaName: string;
  areas: null;
  level: number;
  parentId:number;
}
export interface TreeItem {
  key: number;
  title: string;
  children: Array<TreeItem>
}
export interface Iprops {
  type:'edit' | 'add'; 
  data?: TreeItem | null
}

interface NewProps extends Iprops {
  areaSource: AreaItem[];
  treeData: TreeItem[];
}

const ModalContent = ({type, data, areaSource, treeData}: NewProps, ref) => {
  // data编辑的时候传递的当前节点数据
  // treeData tree的数据也是三级联动的数据
  // areaSource 3715条总体数据
  const [form] = Form.useForm();
  const addKeys = (res:number[], curData:AreaItem | undefined):void => {
    if(curData && curData.parentId !== 0){
      // 找到父级
      const parentNode = areaSource.find(val => val.areaId === curData.parentId);
      parentNode && res.unshift(parentNode.areaId) && addKeys(res, parentNode);
    }
  }
  const forMatCascaderKey = (key: number | undefined) => {
    // 如果是北京 返回 [key]
    // 如果是子级 [parentKey, key]
    if(key){ // 编辑
      const res = [key];
      const curData = areaSource.find(item => item.areaId === key);
      addKeys(res, curData);
      return res;
    }
    return []; // 新增
  }
  // 重新定义ref抛出的值
  useImperativeHandle(ref, () => ({
    form
  }), [form]);
  useEffect(() => {
    // 创建组件的时候回显areaName
    // 回显联级菜单
    form.setFieldsValue({
      areaName: data?.title,
      cascader: forMatCascaderKey(data?.key)
    });
  }, [data]);
  return (  // 弹窗中间的内容
    <Form form={form}>
      <Form.Item
        label="地区名称"
        name="areaName"
        rules={[{ required: true, message: '区域名称不能为空' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="上级地址"
        name="cascader"
      >
        <Cascader 
          fieldNames={{ label: 'title', value: 'key' }}
          options={treeData}
          placeholder={'请选择上级地区'}
          changeOnSelect
        />
      </Form.Item>
    </Form>
  )
}

export default forwardRef(ModalContent);