import { useState, useEffect } from "react"
import { getAreaList, delAreaItem, areaItem, AreaItemParams} from "@/services/sys"
import { Tree, Button, Input, Modal, message } from "antd"
import style from "./index.less"
import { debounce } from "@/utils/utils"
import ModalContent, {AreaItem, TreeItem, Iprops} from "./ModalContent"
const { Search } = Input;

const findChildrenData = (data: AreaItem[], parentId:number):TreeItem[] => {
  return data.filter(val => Number(val.parentId) === parentId && val.parentId !== val.areaId).map(item => {
    return {
      key: item.areaId,
      level: item.level,
      title: item.areaName,
      children: findChildrenData(data, item.areaId)
    }
  })
}
// 格式化treeData数据
const formatAreaData = (data: AreaItem[], level: number):TreeItem[] => {
  return data.filter(val => Number(val.level) === level || val.areaId === val.parentId).map((item:AreaItem) => {
    return {
      key: item.areaId,
      level: item.level,
      title: item.areaName,
      children: findChildrenData(data, item.areaId)
    }
  })
}
const findParentIds = (value:string, sourceArr:AreaItem[]) => {
  // 从3715个值中找到areaName包含value的值
  const res:AreaItem[] = [];
  const curValArr = sourceArr.filter(item => item.areaName?.includes(value));
  // 7 
  const findParent = item => {
    if(item.parentId !== 0 && item.parentId !== item.areaId){ // 还有父级
      // 找父级地址
      const parent = sourceArr.find(val => val.areaId === item.parentId);
      parent && findParent(parent);
    }
    res.findIndex(val => val.areaId === item.areaId) < 0 && res.push(item); // 判断在数组中出现过
  }
  curValArr.map(findParent);
  return res;
}


const Pages = () => {
  // 只在第一请求数据时修改源数据，之后一直没有修改
  const [areaSource, setAreaSource] = useState<AreaItem[]>();
  // 格式化之后的tree的数据
  const [areaList, setAreaList] = useState<TreeItem[]>([]);
  const init = async () => {
    const data = await getAreaList();
    setAreaSource(data); // 设置源数据
    setAreaList(formatAreaData(data, 1)); // 设置tree数据
  }
  const handleClickDel = (nodeData:TreeItem) => {
    Modal.warning({
      title: '删除弹窗',
      content: <div>确定要删除吗？</div>,
      okText: '确定删除',
      onOk: async () => {
        await delAreaItem(nodeData.key);
        message.success('删除成功', 2, () => {
          init();
        })
      }
    });
  }
  const titleRender = (nodeData:TreeItem) => {
    // console.log(nodeData);
    return <div key={nodeData.key} className={style.treeItem}>
      <p>{nodeData.title}</p>
      <div>
        <Button type="link" onClick={() => hanldShowModel({type:'edit', data: nodeData})}>修改</Button>
        <Button type="link" onClick={() => handleClickDel(nodeData)}>删除</Button>
      </div>
    </div>
  }
  // 点击新增和编辑
  const hanldShowModel = ({type, data}: Iprops) => { // data如果是新增弹窗就不存在
    let contentForm = null;
    // 显示弹窗
    const ModalInfo = Modal.info({
      title: type === 'add' ? '新增' : '修改',
      content: <ModalContent ref={(val) => {  // ref是react组件自动转发的可以接受一个函数
        // val就是子组件抛出的实例
        contentForm = val;
      }} type={type} data={data} areaSource={areaSource} treeData={areaList} />,
      cancelText: '取消',
      okText:'确定',
      onOk: async () => { 
        // 获取表单内容 首先拿到form
        const form = contentForm?.form;
        if(form){
          await form.validateFields() // 校验form表单
          const {cascader, areaName} = form.getFieldsValue();
          const params:AreaItemParams = {
            areaId: type === 'add' ? 0 : data?.key,
            areaName,
            areas: null,
            level: type === 'add' ? 2 : data?.level,
            parentId: Array.isArray(cascader) ? cascader[cascader.length - 1] : null
          }
          // 调新增或者编辑接口
          await areaItem(type === 'add' ? 'post' : 'put', params)
          message.success((type === 'add' ? '新增' : '更新') + '操作成功', 2, async () => {
            // 更新列表
            await init();
            // console.log(ModalInfo);
            ModalInfo.destroy() // 关闭弹窗
          });
        }
      }
    })
  }
  const onChange = debounce((e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    if(value){
      const values = findParentIds(value, areaSource); // 找到搜索的匹配结果
      setAreaList(formatAreaData(values || [], 1));
    } else { // 回到tree的初始状态
      setAreaList(formatAreaData(areaSource, 1));
    }
  }, 100)
  // 进入组件
  useEffect(() => {
    init();
  }, [])
  return (
    <div>
      <Search style={{ marginBottom: 8 }} placeholder="请输入搜索内容" onChange={onChange} /><Button onClick={() => hanldShowModel({type:'add'})}>新增</Button>
      <Tree 
        titleRender={titleRender}
        treeData={areaList}
      />
    </div>
  )
}
export default Pages;