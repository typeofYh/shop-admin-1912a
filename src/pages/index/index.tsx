import styles from './index.less';
import { useState, useEffect } from "react"
export default function IndexPage() {
  
  useEffect(() => {
    console.log('index page')
  }, [])
  return (
    <div>
      <h1 className={styles.title}>home page</h1>
    </div>
  );
}
