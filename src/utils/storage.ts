interface ILocale {
  type?: 'local' | 'session';
  name: string;
}
class LocalStore {
  storage: any 
  name: string
  constructor({type = 'local', name} : ILocale){
    this.storage = window[type + 'Storage'];
    this.name = name;
  }
  get(){
    try {
      return JSON.parse(this.storage.getItem(this.name))
    } catch(error){
      return this.storage.getItem(this.name);
    }
  }
  set(value:any){
    console.log(value);
    this.storage.setItem(this.name, JSON.stringify(value));
  }
  remove(){
    this.storage.removeItem(this.name);
  }
}


export default LocalStore;

