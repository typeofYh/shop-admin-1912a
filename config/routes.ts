const hide = {
  // 隐藏子菜单
  hideChildrenInMenu: false,
  // 隐藏自己和子菜单
  hideInMenu: false,
  // 在面包屑中隐藏
  hideInBreadcrumb: false,
}

export default [
  {
    path: '/',
    redirect: '/home'    
  },
  { 
    path: '/home', 
    component: '@/pages/index/index',
    name: '首页',
    wrappers: [
      '@/wrappers/isToken',
    ],
  },
  {
    path: '/prodInfo',
    component: '@/pages/prodInfo/index',
    hideInMenu: false,
    // 隐藏子菜单
    hideChildrenInMenu: false,
  },
  { 
    path: '/login', 
    component: '@/pages/login/index',
    ...hide,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
  },
]