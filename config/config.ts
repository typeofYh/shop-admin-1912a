import { defineConfig } from 'umi';
import routes from "./routes";

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  title: 'bw-电商后台',
  layout: {
    
  },
  fastRefresh: {},
  proxy: {
    '/api': {
      'target': 'https://bjwz.bwie.com/mall4w',
      'changeOrigin': true,
      'pathRewrite': { '^/api' : '' },
    },
  },
});
